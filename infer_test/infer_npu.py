import argparse
from pprint import pprint

import paddle


#
from  uie_predictor import UIEPredictor

parser = argparse.ArgumentParser(description='Paddlenlp for Chinese NER task')
# Required parameters
parser.add_argument("--model_path_prefix", default='./export/inference',
                    type=str, help="The path prefix of inference model to be used.", )
parser.add_argument("--position_prob", default=0.5, type=float,
                    help="Probability threshold for start/end index probabiliry.", )
parser.add_argument("--use_fp16", default=False,action='store_true',
                    help="Whether to use fp16 inference, only takes effect when deploying on gpu.", )
parser.add_argument("--max_seq_len", default=512, type=int,
                    help="The maximum input sequence length. Sequences longer than this will be split automatically.", )
parser.add_argument("--batch_size", default=4, type=int, help="Batch size per GPU for inference.")
parser.add_argument("--device_id", default=0, type=int, help="The GPU device ID.")
args = parser.parse_args()


def load_predictor_model():
    # args = parse_args()
    print('>>>>',args)
    schema =  ['疾病','症状','时间','医院','手术']
    args.device = 'npu'
    args.schema = schema
    predictor = UIEPredictor(args)
    return predictor



if __name__ == "__main__":


    texts = [
                # '被保险人于2006年8月30日在北京天坛医院行脑部瘤手术',
                # '于2017-02-23 09:56:01在北京市顺义区妇幼保健院诊断子宫平滑肌瘤；',
                # '于2017-08-20 18:49:20在北京市顺义区妇幼保健院诊断上呼吸道感染；',
                # '于2017-08-22 00:00:00在北京市顺义区中医医院（北京中医医院顺义医院）诊断肺炎；',
                # '于2017-08-22 09:37:31在北京市顺义区中医医院（北京中医医院顺义医院）诊断发热待查；',
                # '于2017-11-16 09:38:00在北京市顺义区中医医院（北京中医医院顺义医院）诊断反流性食管炎，幽门螺杆菌感染；',
                '于2017-11-16 10:28:02在北京市顺义区中医医院（北京中医医院顺义医院）诊断理气健脾化湿，胃痞，气滞；',
                '于2017-11-27 08:35:42在北京市顺义区中医医院（北京中医医院顺义医院）诊断清气凉营解毒，邪入气营，瘾疹，理气健脾，荨麻疹'
            ]
    predictor = load_predictor_model()
    outputs = predictor.predict(texts)
    for text, output in zip(texts, outputs):
        print("1. Input text: ")
        print(text)
        print("3. Result: ")
        pprint(output)
        print("-----------------------------")
